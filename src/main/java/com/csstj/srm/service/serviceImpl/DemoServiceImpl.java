package com.csstj.srm.service.serviceImpl;

import com.csstj.srm.dao.DemoMapper;
import com.csstj.srm.entity.Demo;
import com.csstj.srm.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DemoServiceImpl implements DemoService {

    @Autowired
    private DemoMapper demoMapper;

    @Override
    public Demo selectByPrimaryKey(int i) {
        return demoMapper.selectByPrimaryKey(1);
    }
}
