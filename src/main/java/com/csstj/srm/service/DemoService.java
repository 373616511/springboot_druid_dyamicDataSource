package com.csstj.srm.service;

import com.csstj.srm.entity.Demo;

public interface DemoService {
    Demo selectByPrimaryKey(int i);
}
