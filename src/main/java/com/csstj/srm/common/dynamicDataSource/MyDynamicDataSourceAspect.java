package com.csstj.srm.common.dynamicDataSource;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 可以在切面切换数据源（比如根据session判断），也可以在servcie代码中设置
 */
@Component
@Aspect
public class MyDynamicDataSourceAspect {

    private final Logger logger = LoggerFactory.getLogger(MyDynamicDataSourceAspect.class);

    @Pointcut("execution( * com.csstj.srm.dao.*.*(..))")
    public void daoAspect() {
        logger.info("pointcut方法执行");
    }

    @Before("daoAspect()")
    public void switchDataSource(JoinPoint point) {
        logger.info("before_pointcut方法执行");
    }

    @After("daoAspect())")
    public void restoreDataSource(JoinPoint point) {
        logger.info("after_pointcut方法执行");
    }
}
