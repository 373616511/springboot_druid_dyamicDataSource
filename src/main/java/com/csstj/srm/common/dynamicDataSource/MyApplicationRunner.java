package com.csstj.srm.common.dynamicDataSource;

import com.csstj.srm.utils.SpringContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * springboot启动之后，执行，spring启动以后最后一个回调，类似于开机启动，也可以在启动main函数后面直接写
 */
@Component
@Order(value = 1)//执行顺序
public class MyApplicationRunner implements ApplicationRunner {
    private final Logger logger = LoggerFactory.getLogger(MyApplicationRunner.class);


    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        logger.info("springboot启动之后，执行");
    }
}
