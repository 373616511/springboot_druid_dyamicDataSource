package com.csstj.srm;

import com.csstj.srm.utils.DataSourceUtil;
import com.csstj.srm.utils.SpringContextUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@MapperScan("com.csstj.srm.dao")
public class SrmApplication {

    private static final Logger logger = LoggerFactory.getLogger(SrmApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(SrmApplication.class, args);
        logger.info("springboot启动成功");
        SpringContextUtil.setApplicationContext(applicationContext);
        DataSourceUtil.initDataSource();
    }
}
